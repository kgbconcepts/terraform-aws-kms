output "key_arn" {
  value = aws_kms_key.default.0.arn
}

output "key_id" {
  value = aws_kms_key.default.0.key_id
}

output "alias_arn" {
  value = aws_kms_alias.default.0.arn
}

output "alias_name" {
  value = aws_kms_alias.default.0.name
}
