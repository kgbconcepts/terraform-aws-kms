variable "enabled" {
  type        = bool
  description = "Set to `false` to prevent the module from creating any resources"
  default     = true
}

variable "region" {
  type        = string
  description = "Set region"
  default     = "us-east-1"
}

variable "name" {
  type        = string
  description = "Application or solution name (e.g. `app`)"
  default     = "example-key"
}

variable "tags" {
  type        = map(string)
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
  default = {
    service = "kms"
  }
}

variable "key_is_enabled" {
  type        = bool
  description = "Specifies whether the key is enabled"
  default     = true
}

variable "key_usage" {
  type        = string
  description = "Specifies the intended use of the key"
  default     = "ENCRYPT_DECRYPT"
}

variable "deletion_window_in_days" {
  type        = string
  description = "Duration in days after which the key is deleted after destruction of the resource"
  default     = "10"
}

variable "enable_key_rotation" {
  type        = bool
  description = "Specifies whether key rotation is enabled"
  default     = true
}

variable "description" {
  type        = string
  description = "The description of the key as viewed in AWS console"
  default     = "Parameter Store KMS master key"
}

variable "alias" {
  type        = string
  description = "The display name of the alias. The name must start with the word `alias` followed by a forward slash"
  default     = ""
}
