resource "aws_kms_key" "default" {
  count                   = var.enabled ? 1 : 0
  is_enabled              = var.key_is_enabled
  key_usage               = var.key_usage
  deletion_window_in_days = var.deletion_window_in_days
  enable_key_rotation     = var.enable_key_rotation
  tags                    = var.tags
  description             = var.description
}

resource "aws_kms_alias" "default" {
  count         = var.enabled ? 1 : 0
  name          = coalesce(var.alias, format("alias/%v", var.name))
  target_key_id = aws_kms_key.default.0.id
}
